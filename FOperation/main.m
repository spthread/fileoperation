//
//  main.m
//  FOperation
//
//  Created by Surendra Patel on 09/08/13.
//  Copyright (c) 2013 Surendra Patel. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **)argv);
}
