//
//  FOAppDelegate.m
//  FOperation
//
//  Created by Surendra Patel on 09/08/13.
//  Copyright (c) 2013 Surendra Patel. All rights reserved.
//

#import "FOAppDelegate.h"

@implementation FOAppDelegate

- (void)dealloc
{
    [super dealloc];
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    NSString *iconPath = [[NSBundle mainBundle] pathForResource:@"FOperation" ofType:@"icns"];
    [self.window setRepresentedURL:[[[NSURL alloc] initFileURLWithPath:iconPath] autorelease]];
}

- (BOOL)application:(NSApplication *)sender openFile:(NSString *)filename
{
    [self handleDropOnThisApplication:[NSURL fileURLWithPath:filename]];
    return NO;
}

- (void)handleDropOnThisApplication:(NSURL *)pathString
{
    NSInteger btn = NSRunAlertPanel(@"Permanent Delete", @"Changes will not be undone !!\nDo you want to continue ?",@"Continure",@"Cancel", nil);
    if (btn) {
        BOOL result = [[NSWorkspace sharedWorkspace] performFileOperation:NSWorkspaceDestroyOperation
                                                                   source:[[pathString relativePath] stringByDeletingLastPathComponent]
                                                              destination:[pathString relativePath]
                                                                    files:[NSArray arrayWithObject:[pathString lastPathComponent]]
                                                                      tag:nil];
        if (result) {
            NSLog(@"Deleted file at - %@",pathString);
        }
        else
            NSLog(@"Should delete file at - %@",pathString);
    }
}

- (NSMenu *)applicationDockMenu:(NSApplication *)sender
{
    NSMenu *menu = [[[NSMenu alloc] init] autorelease];
    [menu addItemWithTitle:@"Open Window" action:@selector(openOperationWindow:) keyEquivalent:@""];
    return menu;
}

-(IBAction)openOperationWindow:(id)sender
{
    [self.window makeKeyAndOrderFront:nil];
}

- (BOOL)windowShouldClose:(NSNotification *)notification
{
    [self.window orderOut:nil];
    return NO;
}

- (BOOL)applicationShouldHandleReopen:(NSApplication *)theApplication hasVisibleWindows:(BOOL)flag
{
    [self.window makeKeyAndOrderFront:nil];
    return NO;
}

@end
