//
//  FOFileOperationController.m
//  FOperation
//
//  Created by Surendra Patel on 09/08/13.
//  Copyright (c) 2013 Surendra Patel. All rights reserved.
//

#import "FOFileOperationController.h"

@interface FOFileOperationController ()

@end

@implementation FOFileOperationController

@synthesize tableView = _tableView;
@synthesize resultLabel = _resultLabel;
@synthesize destinationPath = _destinationPath;
@synthesize fileOperation = _fileOperation;
@synthesize filesArray = _filesArray;
@synthesize filesArrayController = _filesArrayController;

- (id) init
{
    self = [super init];
    if (self)
    {
        self.filesArray = [[NSMutableArray alloc] init];
        self.fileOperation = @"Copy";
    }
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidLauched) name:NSApplicationDidFinishLaunchingNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(performFileOperation:) name:@"FOFileOperationWhenDragged" object:nil];
    return self;
}

-(void)applicationDidLauched
{
    [self.tableView registerForDraggedTypes:[NSArray arrayWithObjects: NSStringPboardType,NSFilenamesPboardType,NSTabularTextPboardType,nil]];
}

#pragma mark ----File Operation Methods----
- (IBAction)chooseFileForOperation:(id)sender
{
    NSOpenPanel *open = [NSOpenPanel openPanel];
    [open setAllowsMultipleSelection:YES];
    [open setFrame:[[[sender superview] window] frame] display:NO];
    [open setCanChooseDirectories:YES];
    [open beginSheetModalForWindow:[[sender superview] window] completionHandler:^(NSInteger result) {
        [self didEndSheetWindow:open result:result userInfo:nil];
    }];
}

-(void)didEndSheetWindow:(NSOpenPanel *)inOpenPanel result:(NSInteger)inResult userInfo:(NSDictionary *)userInfo
{
    [inOpenPanel orderOut:self];
    [inOpenPanel close];
    if (inResult)
        [self updateFilesArrayDataWithPaths:[inOpenPanel URLs]];
}

- (IBAction)chooseDestinationPath:(id)sender
{
    NSOpenPanel *open = [NSOpenPanel openPanel];
    [open setFrame:[[[sender superview] window] frame] display:NO];
    [open setCanChooseDirectories:YES];
    [open setCanChooseFiles:NO];
    [open beginSheetModalForWindow:[[sender superview] window] completionHandler:^(NSInteger result) {
        if (result)
            self.destinationPath.stringValue = [[open URL] absoluteString];
    }];
}

- (IBAction)operationAction:(id)sender
{
    _fileOperation = [[sender selectedCell] title];
    
    NSLog(@"Action %@ selected",_fileOperation);
}

- (IBAction)clear:(id)sender
{
    [self willChangeValueForKey:@"filesArray"];
    if([_filesArray count])
        [_filesArray removeAllObjects];
    [self didChangeValueForKey:@"filesArray"];
    [self.tableView reloadData];
}

- (IBAction)rowSelectedOrUnselected:(id)sender
{
    NSLog(@"Action on Row index %@",sender);
    [self willChangeValueForKey:@"filesArray"];
    [self.filesArray removeObjectAtIndex:[[self.tableView selectedRowIndexes] firstIndex]];
    [self didChangeValueForKey:@"filesArray"];
}

- (void) updateFilesArrayDataWithPaths:(NSArray *)inSelectedFilePaths
{
    if (!self.filesArray) {
        self.filesArray = [[NSMutableArray alloc] init];
    }
    [self willChangeValueForKey:@"filesArray"];
    for (NSURL *url in inSelectedFilePaths)
    {
        NSMutableDictionary *fileDict = [NSMutableDictionary dictionary];
        [fileDict setObject:[NSNumber numberWithBool:YES] forKey:@"checkBoxState"];
        [fileDict setObject:url forKey:@"filePath"];
        [self.filesArray addObject:fileDict];
    }
    [self didChangeValueForKey:@"filesArray"];
    NSLog(@"%@",self.filesArray);
    [self.tableView reloadData];
}

-(IBAction)performOperation:(id)sender
{
    NSString *operationName = [self fileOperationString];
    if ([operationName isEqualToString:@"destroy"] && [_filesArray count])
    {
        NSBeginAlertSheet(@"Permanent Delete", @"Continue", @"Cancel", nil, [[sender superview] window], self, @selector(sheetDidEnd:returnCode:contextInfo:), nil, nil,@"Changes will not be undone !!\nDo you want to continue ?");
    }
    else {
     [self performFileOperation];   
    }
}

-(void) performFileOperation
{
    NSInteger result = 0;
    BOOL fileExist = YES;
    for (NSDictionary *pathString in _filesArray)
    {
        fileExist = [[NSFileManager defaultManager] fileExistsAtPath:[[pathString valueForKey:@"filePath"] relativePath]];
        if (!fileExist)
            continue;
        
        [[NSWorkspace sharedWorkspace] performFileOperation:[self fileOperationString]
                                                     source:[[[pathString valueForKey:@"filePath"] relativePath] stringByDeletingLastPathComponent]
                                                destination:[[NSURL URLWithString:self.destinationPath.stringValue] relativePath]
                                                      files:[NSArray arrayWithObject:[[pathString valueForKey:@"filePath"] lastPathComponent]]
                                                        tag:&result];
        
        #warning Check here if all operation are success or any single failed in bettween.
    }
    if (result >= 0)
    {
        NSLog(@"File operation '%@' success",_fileOperation);
        [self.resultLabel setStringValue:[NSString stringWithFormat:@"%ld File(s) '%@'",[self.filesArray count],_fileOperation]];
    }
    else{
        NSBeginAlertSheet(@"Alert", @"OK", nil, nil, [[self.tableView superview] window], self, nil, nil, nil,@"Cannot perform Operation : %@",_fileOperation);
        NSLog(@"Cannot perform last Operation : %@",_fileOperation);
    }
}

- (void)sheetDidEnd:(NSWindow *)sheet returnCode:(NSInteger)returnCode contextInfo:(void *)contextInfo
{
    if (returnCode)
    {
        [self performFileOperation];
    }
}

-(NSString*)fileOperationString
{
    NSDictionary *fileOperationStrings = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"FileOperationStrings" ofType:@"plist"]];
    return [fileOperationStrings valueForKey:self.fileOperation];;
}

#pragma mark ----Table View Drop Handling Methods----

- (BOOL)tableView:(NSTableView *)tableView acceptDrop:(id <NSDraggingInfo>)info row:(NSInteger)row dropOperation:(NSTableViewDropOperation)dropOperation
{
    NSMutableArray *arrayOfDraggedFiles = [NSMutableArray array];
    for (NSString *urlString in [[info draggingPasteboard] propertyListForType:NSFilenamesPboardType]) {
        [arrayOfDraggedFiles addObject:[NSURL fileURLWithPath:urlString]];
    }
    [self updateFilesArrayDataWithPaths:arrayOfDraggedFiles];
    return YES;
}

- (NSDragOperation)tableView:(NSTableView *)tableView validateDrop:(id <NSDraggingInfo>)info proposedRow:(NSInteger)row proposedDropOperation:(NSTableViewDropOperation)dropOperation
{
    NSUInteger dragOperation = NSDragOperationNone;
    NSString *droppedFilePath = [[[info draggingPasteboard] propertyListForType:@"NSFilenamesPboardType"] objectAtIndex:0];
    NSString *bundlePath = [[NSBundle mainBundle] bundlePath];
    if (![bundlePath rangeOfString:droppedFilePath].length) {
        if (![droppedFilePath rangeOfString:bundlePath].length) {
            dragOperation = NSDragOperationCopy;
        }
    }
    return dragOperation;
}

#pragma mark ----Memory Methods----
-(void)dealloc
{
    self.filesArray = nil;
    self.fileOperation = nil;
    [self.fileOperation release];
    [self.filesArray release];
    [[NSNotificationCenter defaultCenter] removeObject:self];
    [super dealloc];
}
@end
