//
//  FOAppDelegate.h
//  FOperation
//
//  Created by Surendra Patel on 09/08/13.
//  Copyright (c) 2013 Surendra Patel. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface FOAppDelegate : NSObject <NSApplicationDelegate,NSWindowDelegate>

@property (assign) IBOutlet NSWindow *window;

- (void)handleDropOnThisApplication:(NSString *)pathString;
@end
