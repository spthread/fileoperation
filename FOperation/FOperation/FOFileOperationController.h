//
//  FOFileOperationController.h
//  FOperation
//
//  Created by Surendra Patel on 09/08/13.
//  Copyright (c) 2013 Surendra Patel. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FOFileOperationController : NSObject <NSToolbarDelegate,NSTableViewDataSource,NSTableViewDelegate>
{
    NSString *fileOperation;
    NSMutableArray *filesArray;
}

@property (assign) IBOutlet NSTableView *tableView;
@property (assign) IBOutlet NSTextField *resultLabel;
@property (assign) IBOutlet NSTextField *destinationPath;
@property (readwrite,copy) NSString *fileOperation;
@property (readwrite, retain) NSMutableArray *filesArray;

@property (assign) IBOutlet NSArrayController *filesArrayController;

-(void) updateFilesArrayDataWithPaths:(NSArray *)inSelectedFilePaths;
-(void)didEndSheetWindow:(NSOpenPanel *)inOpenPanel result:(NSInteger)inResult userInfo:(NSDictionary *)userInfo;

@end
